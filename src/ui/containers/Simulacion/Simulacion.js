import React from 'react';
// Actions
import { Card, CardTitle, Input } from '@design-system-coopeuch/web';

const Simulacion = () => {
  return (
    <div className="container">
      <br />
      <Card className="bg-body">
        <CardTitle
          subtitle="Completa los siguientes datos para conocer tu capacidad financiera y ofrecerte una experiencia perzonalizada."
          title="Cuéntanos sobre ti"
        />
        <div className="row">
          <div className="col-md-4">
            <Input
              assistText={[
                {
                  text: 'Sin puntos, ni guión',
                  type: 'assist',
                },
              ]}
              label="RUT"
            />
          </div>
        </div>
        <hr />
        <div className="row">
          <div className="col-md-4">
            <Input label="Tu primer nombre" />
          </div>
          <div className="col-md-4">
            <Input label="Tu apellidos" />
          </div>
        </div>
      </Card>
      <br />
      <Card className="bg-body">
        <CardTitle title="Detalles de la propiedad" />
        <h5>Propiedad: Departamento</h5>
      </Card>
      <br />
      <Card className="bg-body">
        <CardTitle title="Términos de privacidad" />
        <div className="row">
          <div className="col-md-12">
            <p>
              {`Autorizo a Coopeuch a procesar mis datos personales que se solicitarán más adelante,
              para que me proporcione al término de este proceso una simulación de crédito
              hipotecario no vinculante para Coopeuch. En particular, Coopeuch podrá recolectar,
              almacenar, confrontar con bases de datos propias o de terceros, públicas o privadas y,
              en general, procesar mis siguientes datos: (i) RUT, nombre y apellidos, para verificar
              mi identidad y otorgarme un trato personalizado; (ii) RUT, renta mensual fija o
              variable, años de antigüedad laboral y si cuento con contrato de trabajo a plazo fijo
              o indefinido, para conocer mi comportamiento y capacidad crediticia y de pagos; (iii)
              N° telefónico y correo electrónico, para contactarme e iniciar tratativas de una
              eventual contratación de credito hipotecario; (iv) conocer mi calidad de beneficiario
              de subvención "Pago Oportuno" del MINVU, para descontar esta subvención del dividendo
              simulado; y (v) la información contenida en los documentos aportados por mi, para
              priorizar mi evaluación.`}
            </p>
          </div>
        </div>
      </Card>
    </div>
  );
};

export default Simulacion;
