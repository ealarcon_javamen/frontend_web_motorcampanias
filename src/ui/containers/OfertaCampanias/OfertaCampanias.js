import React, { useState } from 'react';
// Actions
import 'chart.js/auto';
import {
  Stepper,
  Button,
  Card,
  CardTitlePrimary,
  CardDropdown,
  Message,
} from '@design-system-coopeuch/web';
import { Doughnut } from 'react-chartjs-2';
import IconReliq1 from '../../assets/img/reliquidacion01.svg';
import IconReliq2 from '../../assets/img/reliquidacion02.svg';
import IconReliq3 from '../../assets/img/reliquidacion03.svg';

const OfertaCampanias = () => {
  const data = {
    datasets: [
      {
        data: [589000, 2811280],
        backgroundColor: ['rgba(50, 167, 52, 1)', 'rgba(203, 170, 37, 1)'],
        borderColor: ['rgba(199, 235, 188, 1)', 'rgba(241,230,171, 1)'],
        hoverOffset: 20,
        radius: 160,
        offset: 10,
        weight: 5,
      },
    ],
  };

  const [opened, isOpened] = useState(false);

  const activar = () => {
    console.log(opened);
    if (opened) {
      isOpened(false);
    } else {
      isOpened(true);
    }
  };
  const [opened2, isOpened2] = useState(false);

  const activar2 = () => {
    console.log(opened2);
    if (opened2) {
      isOpened2(false);
    } else {
      isOpened2(true);
    }
  };

  return (
    <div className="container">
      <br />
      <Stepper
        currentStep={1}
        steps={[
          {
            name: 'Ajusta tu oferta',
            step: 1,
          },
          {
            name: 'Revisa tu oferta',
            step: 2,
          },
          {
            name: 'Contratos',
            step: 3,
          },
          {
            name: 'Mandato Pagaré',
            step: 4,
          },
          {
            name: 'Desembolso y Comprobante',
            step: 5,
          },
        ]}
      />
      <Card className="bg-body">
        <div className="row">
          <div className="col-md-9">
            <CardTitlePrimary title="Oferta de crédito con reliquidación" />
          </div>
          <div className="col-md-3 derechatext">
            <Button onClick={() => {}} prefix="line-write" variant="text">
              Personalizar oferta
            </Button>
          </div>
        </div>
        <nav className="navbar bluebg pt-4 pb-4">
          <div className="col-md-3 pl-7">
            <p>Monto a otorgar</p>
            <h4 style={{ color: 'white' }}>$3.985.623</h4>
          </div>
          <div className="col-md-3 pl-7">
            <p>Saldo en tu cuenta</p>
            <h4 style={{ color: 'white' }}>$589.000</h4>
          </div>
          <div className="col-md-3 pl-7">
            <p>Plazo</p>
            <h4 style={{ color: 'white' }}>72 meses</h4>
          </div>
          <div className="col-md-3 pl-7">
            <p>Valor cuota</p>
            <h4 style={{ color: 'white' }}>$104.000</h4>
          </div>
        </nav>

        <nav className="navbar graybg pt-4 pb-4">
          <div className="row pl-7 pr-7">
            <div className="col-md-12">
              <p>Total seguros contratados: $188.949</p>
            </div>
            <br />
            <br />
            <div className="col-md-12">
              <Card className="whitebg bordernone">
                <CardTitlePrimary subtitle="Desgravamen" />
              </Card>
            </div>
            <br />
            <br />
            <br />
            <br />
            <div className="col-md-12">
              <Message
                description="Por políticas de riesgo crediticio, el seguro desgravamen es obligatorio. Para contratar un seguro de otra empresa aseguradora, éste debe cumplir las condiciones establecidas en el contrato de crédito que verás en los pasos siguientes y gestionar este crédito en sucursal."
                type="info"
              />
            </div>
          </div>
        </nav>
        <div className="whitebg">
          <CardDropdown
            opened={opened}
            title='¿Como se compone el "Monto a otorgar"?'
            onClick={activar}
          >
            <div className="row">
              <div className="col-md-4 pl-7">
                <Doughnut
                  data={data}
                  plugins={[
                    {
                      beforeDraw(chart) {
                        const { width } = chart;
                        const { height } = chart;
                        const { ctx } = chart;
                        ctx.restore();
                        const fontSize = (height / 300).toFixed(2);
                        ctx.font = `bold ${fontSize}em  Ubuntu`;
                        ctx.textBaseline = 'top';
                        const text = '$3.985.623';
                        const textX = Math.round((width - ctx.measureText(text).width) / 2);
                        const textY = height / 2;
                        ctx.fillText(text, textX, textY - 20);
                        ctx.font = `${fontSize}em  Ubuntu`;
                        const text2 = 'Monto a otorgar';
                        ctx.fillText(
                          text2,
                          Math.round((width - ctx.measureText(text2).width) / 2),
                          height / 2 + 10
                        );
                        ctx.save();
                      },
                    },
                  ]}
                />
              </div>

              <div className="col-md-7 pl-7">
                <br />
                <p style={{ 'font-size': '19px' }}>
                  El monto a otorgar de esta oferta de crédito se compone por el monto destinado a
                  <b> reliquidar tus créditos vigentes</b>
                  {` y otro monto para tu disposición que
                  sera desembolsado en tu cuenta vista coopeuch en la proporción que muestra el
                  siguiente gráfico:`}
                </p>
                <br />
                <br />
                <ul>
                  <li className="uli1">
                    {' '}
                    <p style={{ 'font-size': '19px' }}>
                      {` Saldo en tu cuenta: `}
                      <b>$589.000</b>
                    </p>
                  </li>
                  <br />
                  <li className="uli2">
                    {' '}
                    <p style={{ 'font-size': '19px' }}>
                      {`Reliquidación créditos vigentes: `}
                      <b>$2.811.280</b>
                    </p>
                  </li>
                </ul>
              </div>
            </div>
          </CardDropdown>
          <CardDropdown
            opened={opened2}
            title="¿Cómo funcionan las ofertas de crédito con reliquidación?"
            onClick={activar2}
          >
            <div className="row">
              <div className="col-md-4">
                <Card className="bordernone">
                  <div className="centrartext">
                    <a className="navbar-brand" href="/">
                      <img width="32" src={IconReliq1} alt="" loading="lazy" />
                    </a>
                  </div>
                  <CardTitlePrimary
                    className="centrartext"
                    subtitle="Obtendrás una nueva oferta a partir de tus créditos vigentes."
                    title="Se reestructuran tus créditos"
                  />
                </Card>
              </div>
              <div className="col-md-4">
                <Card className="bordernone">
                  <div className="centrartext">
                    <a className="navbar-brand" href="/">
                      <img width="32" src={IconReliq2} alt="" loading="lazy" />
                    </a>
                  </div>
                  <CardTitlePrimary
                    className="centrartext"
                    subtitle="Tu nuevo crédito no aumentará tu carga financiera actual."
                    title="Se mantiene tu cuota actual"
                  />
                </Card>
              </div>
              <div className="col-md-4">
                <Card className="bordernone">
                  <div className="centrartext">
                    <a className="navbar-brand" href="/">
                      <img width="32" src={IconReliq3} alt="" loading="lazy" />
                    </a>
                  </div>
                  <CardTitlePrimary
                    className="centrartext"
                    subtitle="Obtendrás un monto líquido adicional en tu cuenta vista."
                    title="Saldo en tu cuenta"
                  />
                </Card>
              </div>
            </div>
          </CardDropdown>
        </div>
      </Card>
    </div>
  );
};

export default OfertaCampanias;
