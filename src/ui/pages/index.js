import React from 'react';
import { HashRouter, Switch, Route } from 'react-router-dom';
import { Helmet } from 'react-helmet';

// Containers
import Home from '../containers/Home/Home';
import OfertaCampanias from '../containers/OfertaCampanias/OfertaCampanias';

const App = () => (
  <>
    <Helmet>
      <title>Cooperativa | Coopeuch</title>
    </Helmet>
    <HashRouter>
      <Switch>
        <Route path="/" exact component={Home} />
        <Route path="/OfertaCampanias" exact component={OfertaCampanias} />
      </Switch>
    </HashRouter>
  </>
);

export default App;
