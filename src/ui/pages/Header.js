import React from 'react';
// import { withRouter } from 'react-router-dom';
// import { connect } from 'react-redux';
import styled from 'styled-components';
import logo from '../assets/img/coopeuch2.svg';

const Header = () => {
  const Nav = styled.nav`
    background-color: #e81e2b;
  `;
  return (
    <header>
      <Nav className="navbar p-4">
        <a className="navbar-brand" style={{ 'margin-left': '67px' }} href="/">
          <img width="157" src={logo} alt="" loading="lazy" />
        </a>
      </Nav>
    </header>
  );
};

export default Header;
