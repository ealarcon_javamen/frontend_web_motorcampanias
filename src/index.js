import React from 'react';
import ReactDOM from 'react-dom';
import { Provider, ReactReduxContext } from 'react-redux';
import 'bootstrap';
import './main.scss';
import '@design-system-coopeuch/web/dist/index.css';
import '@design-system-coopeuch/assets/css/custom.css';
import { TitleSection } from '@design-system-coopeuch/web';
import Header from './ui/pages/Header';
import GlobalStyles from './ui/assets/Theme';

// Configuration
import store from './application/config/store';
// Main app
import App from './ui/pages';

ReactDOM.render(
  <Provider store={store} context={ReactReduxContext}>
    <React.StrictMode>
      <GlobalStyles />
      <Header />
      <TitleSection
        prefix="old-line-logout"
        label="Contrata tu crédito de consumo con reliquidación"
      />
      <div className="container">
        <div className="">
          <br />
        </div>
        <App />
      </div>
    </React.StrictMode>
  </Provider>,
  document.getElementById('root')
);
